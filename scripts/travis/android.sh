#! /bin/bash
[ ! -d "$DOCKER_VOLUMES_CACHE/android-sdk" ] && mkdir -p $DOCKER_VOLUMES_CACHE/android-sdk

cat 3rdparty/distfiles/tarballs/android-sdk/android-sdk.tar.gz.part* > $DOCKER_VOLUMES_CACHE/android-sdk/android-sdk.tar.gz

tar xzvf $DOCKER_VOLUMES_CACHE/android-sdk/android-sdk.tar.gz
rm $DOCKER_VOLUMES_CACHE/android-sdk/android-sdk.tar.gz

name="$(echo "$*" | sed -re 's:\S+/([^/]+):\1:')"
env=( )
while :
do
    case "$1" in
        *=*)
            env+=( -e "$1" )
            shift
            ;;
        *)
            break
            ;;
    esac
done
( set -x;
  docker run --rm -t \
         --mount type=volume,dst=/sdk,volume-driver=local,volume-opt=type=none,volume-opt=o=bind,volume-opt=device=$DOCKER_VOLUMES_CACHE/android-sdk/_data \
         -v "${TRAVIS_BUILD_DIR:-`pwd`}":/opt/roc \
         -w /opt/roc \
         --privileged \
         "${env[@]}" \
         rocstreaming/env-android \
         "$@" )
error=$?
if [ $error = 0 ]
then
  printf '%-90s \e[1;32m%s\e[m\n' "$name" "OK" >> build.status
else
  printf '%-90s \e[1;31m%s\e[m\n' "$name" "FAILED" >> build.status
fi
exit $error
